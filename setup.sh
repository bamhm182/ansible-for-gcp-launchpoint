#!/bin/bash

main() {
    setup $@
    update
    passwd_user
    run_ansible
    prompt_reboot
}

setup() {
    /usr/bin/clear

    head_1 "Setting Up"

    BASE_DIR="$(dirname $(realpath ${BASH_SOURCE[0]}))"
    SCRIPT="$(realpath ${BASH_SOURCE[0]})"

    if [ "$EUID" != "0" ]; then
        exec /usr/bin/sudo /bin/bash "${SCRIPT}" $@
    fi
    
    choose_playbook
}

choose_playbook() {
    head_2 "Choosing the Playbook"

    PLAYBOOK="${1:-${SUDO_USER:-_default}}"

    if [[ ! -f "${BASE_DIR}/playbooks/${PLAYBOOK}.yml" ]]; then
        if [[ ! -z "${1}" ]]; then
            echo "WARNING: playbooks/${PLAYBOOK}.yml does not exist."
            printf "Use playbooks/_default.yml? [y/N]: "
            read choice
            case "${choice}" in
                Y*|y*)
                    PLAYBOOK="_default"
                    ;;
                *)
                    exit
                    ;;
            esac
        else
            PLAYBOOK="_default"
        fi
    fi

    echo "playbooks/${PLAYBOOK}.yml will be installed..."

    if [[ "${PLAYBOOK}" != "${SUDO_USER}" ]]; then
        echo "Waiting 5 seconds... Press Ctrl+C to abort..."
        sleep 5
    fi
}

update() {
    head_1 "Updating System"

    /usr/bin/apt update && /usr/bin/apt dist-upgrade -y && /usr/bin/apt autoremove --purge -y
}

passwd_user() {
    if [[ "$(grep "${SUDO_USER}" /etc/passwd)" == "" ]]; then
    	echo $(getent passwd | grep ${SUDO_USER}) >> /etc/passwd
    fi
}

run_ansible() {
    head_1 "Running Ansible"
    if [ -x "/usr/bin/dpkg" ]; then
        /usr/bin/dpkg -s ansible > /dev/null 2>&1
        if [[ "$?" -ne "0" ]]; then
            /usr/bin/apt update && /usr/bin/apt install ansible -y
        fi
    fi

    head_2 "Running Playbooks"
    /usr/bin/grep "${SUDO_USER}" /etc/sudo* -R | grep "NOPASSWD" > /dev/null 2>&1

    if [[ "$?" -eq 0 ]]; then
    	sudo -u ${SUDO_USER} /usr/bin/ansible-playbook -c local -l localhost "${BASE_DIR}/playbooks/${PLAYBOOK}.yml"
    else
    	sudo -u ${SUDO_USER} /usr/bin/ansible-playbook -c local -l localhost "${BASE_DIR}/playbooks/${PLAYBOOK}.yml" --ask-become-pass
    fi
}

prompt_reboot() {
    printf "Odds are many things about your system just changed.\nWould you like to reboot now? [N]: "
    read choice
    case "${choice}" in
        Y*|y*)
            echo "The system is now rebooting."
            reboot
            ;;
        *)
            echo "The system will NOT reboot now. It may be a good idea to reboot soon."
            ;;
    esac
}

head_1() {
    /usr/bin/echo -e "==============================\n$1\n=============================="
}

head_2() {
    /usr/bin/echo -e "-------------------\n$1\n-------------------"
}

main $@
     
